//! Parse a configuration and run each iteration
//!
//! `Configuration` structs can be created with `Configuration::parse` and then
//! run with `Configuration::run`.

use std::collections::HashMap;
use std::io::Command;
use std::io::{File, IoResult};
use std::io::fs::PathExtensions;
use std::hash::Hash;

use rustc_serialize::json;

type Options = HashMap<String, String>;
type Arguments = Vec<String>;

fn merge(into: &mut Options, from: &Options) {
    for (key, value) in from.iter() {
        into.insert(key.clone(), value.clone());
    }
}

#[derive(RustcDecodable, Clone)]
pub struct Iteration {
    options: Option<Options>,
    arguments: Option<Arguments>
}

impl Iteration {
    /// Returns a `PreparedIteration`, which doesn't need further input
    fn prepare(&self, conf: &Configuration) -> PreparedIteration {
        let command = conf.command.clone();

        // Options are merged from the configuration and iteration, with the
        // iteraction overwriting options from the configuration
        let mut options: Options = HashMap::new();
        merge(&mut options, &conf.options.clone().unwrap_or(HashMap::new()));
        merge(&mut options, &self.options.clone().unwrap_or(HashMap::new()));

        let mut arguments = match self.arguments {
            Some(ref args) => args.clone(),
            None => Vec::new()
        };

        return PreparedIteration {
            command: command,
            options: options,
            arguments: arguments
        }
    }
}

pub struct PreparedIteration {
    command: String,
    options: Options,
    arguments: Arguments
}

impl PreparedIteration {
    fn command(&self) -> Command {
        let mut command = Command::new(self.command.clone());

        for (name, value) in self.options.iter() {
            command.args(&[name, value]);
        }

        for argument in self.arguments.iter() {
            command.arg(argument);
        }

        return command;
    }

    fn print_output(output: Vec<u8>) {
        print!("{}", String::from_utf8_lossy(output.as_slice()));
    }

    fn run(&self) {
        let command = self.command();

        println!("$ {}", command);

        // Run the process and exit if it fails
        let mut result = command.output().unwrap();

        PreparedIteration::print_output(result.output);

        if !result.status.success() {
            println!("STDERR for `{}`:", command);
            PreparedIteration::print_output(result.error);
            panic!("Process `{}` exited with a non-zero code", command);
        }
    }
}

#[derive(RustcDecodable, Clone)]
pub struct Configuration {
    command: String,
    options: Option<Options>,
    iterations: Vec<Iteration>
}

impl Configuration {
    /// Reads a configuration from `path` and returns a `Configuration` struct
    pub fn parse(path: Path) -> IoResult<Configuration> {
        // Assert the path describes a file
        let display = path.display();
        assert!(path.exists(), format!("Path {} does not exist", display));
        assert!(path.is_file(), format!("Path {} is not a file", display));

        // Parse the file from JSON into a `Configuration` struct
        let contents = try!(File::open(&path).read_to_string());
        return Ok(json::decode(contents.as_slice()).unwrap());
    }

    /// Runs each iteraction
    pub fn run(&self) {
        for iteration in self.iterations.iter() {
            iteration.prepare(self).run();
        }
    }
}
