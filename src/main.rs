/// Parses command line arguments to run clack


extern crate getopts;
extern crate "rustc-serialize" as rustc_serialize;

#[not(warn(unstable))]
pub mod configuration;

fn usage(program: String, opts: &[getopts::OptGroup]) {
    let message = format!("Usage: {} [options] FILE", program.as_slice());
    print!("{}", getopts::usage(message.as_slice(), opts));
}

fn main() {
    let args: Vec<String> = std::os::args();
    let program = args[0].clone();

    let opts = &[
        getopts::optflag("h", "help", "print this help menu")
    ];

    let matches = match getopts::getopts(args.tail(), opts) {
        Ok(m) => { m }
        Err(e) => { panic!(e.to_string()) }
    };

    if matches.opt_present("h") || matches.free.is_empty() {
        usage(program, opts);
        return;
    }

    for input in matches.free.iter() {
        configuration::Configuration::parse(Path::new(input)).unwrap().run();
    }
}
